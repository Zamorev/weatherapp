//
//  CurrentWeather.swift
//  WeatherMoscow
//
//  Created by Pavel on 18/01/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
    var _cityName: String!
    var _date: String!
    var _weatherType: String!
    var _currentTemperature: Double!
    
    var cityName: String {
        if _cityName == nil {
            _cityName = "Город"
        }
        return _cityName
    }
    
    // дату берем с устройства пользователя
    var date: String {
        if _date == nil {
            _date = ""
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date = "Сегодня \(currentDate)"
  
        return _date

    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = "Zzzz"
        }
        return _weatherType
    }
    
    var currentTemperature: Double {
        if _currentTemperature == nil {
            _currentTemperature = 0.0
        }
        return _currentTemperature
    }
    
    func downloadWeatherDetails(completed: DownloadComplete) {
        //AlamoFire download
        
//        let currentWeatherURL = URL(string: CURRENT_WEATHER_URL) / без этой строчки работает
        Alamofire.request(CURRENT_WEATHER_URL).responseJSON {
            response in
            let result = response.result
//            print(result) / было
//            print(response)
            
            // ниже мы проходимся по выдаче JSON и ищем совпадения на примере "name"
            if let dict = result.value as? Dictionary<String, AnyObject> {
                if let name = dict["name"] as? String {
                    self._cityName = name.capitalized
                    print(self._cityName)
                }
                
                // далее мы идем вглубь выдачи и заходим в раздел на примере "weather"
                if let weather = dict["weather"] as? [Dictionary<String,AnyObject>] {
                    
                    if let main = weather[0]["main"] as? String {
                        self._weatherType = main.capitalized
                        print(self._weatherType)
                    }
                }
                
                if let main = dict["main"] as? Dictionary<String, AnyObject> {
                    
                    if let currentTemperature = main["temp"] as? Double {
                        
                        // перевод градусов из келвинов в целсия
                        let kelvinToCelcium = (currentTemperature - 273.15)
                        
                        self._currentTemperature = kelvinToCelcium
//                        print("Температура:", self._currentTemperature)
                       
                    }
                }
                
            }
            
        }
        completed()
    }
    
}




