//
//  Constants.swift
//  WeatherMoscow
//
//  Created by Pavel on 18/01/2017.
//  Copyright © 2017 TapTop. All rights reserved.
//

import Foundation

let BASE_URL = "http://api.openweathermap.org/data/2.5/weather?"
let LATITUDE = "lat="
let LONGITUDE = "&lon="
let APP_ID = "&appid="
let API_KEY = "4bd32fb9b3cdab679a37f2dc149a57d6"

typealias DownloadComplete = () -> ()

let CURRENT_WEATHER_URL = "\(BASE_URL)\(LATITUDE)-36\(LONGITUDE)123\(APP_ID)\(API_KEY)"
